import configRaw from './config.json'

interface IConfig {
  extensions: {
    endpoints: {
      [name: string]: {
        url: string
        headers: object
      }
    }
  }
}

export interface ITarget {
  label: string
  url: string
  subdomains: boolean
}

const configSource: IConfig = configRaw

export const config = [
  {
    name: 'prod',
    label: 'doveze.cz',
    targets: [
      {
        label: 'doveze',
        url: 'https://doveze.cz',
        subdomains: true,
      },
      {
        label: 'wificisnik',
        url: 'https://bez-fronty.cz',
        subdomains: true,
      },
    ] as ITarget[],
    endpoint: configSource.extensions.endpoints.prod,
  },
  {
    name: 'dev',
    targets: [
      {
        label: 'doveze',
        url: 'https://doveze.dev.speedlo.cloud',
        subdomains: true,
      },
      {
        label: 'wificisnik',
        url: 'https://bez-fronty.dev.speedlo.cloud',
        subdomains: true,
      },
      { label: 'local', url: 'http://localhost:3000', subdomains: false },
    ] as ITarget[],
    endpoint: configSource.extensions.endpoints.dev,
  },
]
