declare module '*.json' {
  const value: any
  export default value
}

declare type ReactNode = React.ReactElement<any> | null

declare type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>

declare module 'react-masonry-layout'
