import { CssBaseline, Typography } from '@material-ui/core'
import { DeveloperBoard } from '@material-ui/icons'
import React from 'react'

import { AppContainer } from 'components/AppContainer'
import { AppContent } from 'components/AppContent'
import { Deployments } from 'components/Deployments'
import { EndpointDrawer } from 'components/EndpointDrawer'
import { TopBar } from 'components/TopBar'

interface IProps {}

export const App: React.SFC<IProps> = () => (
  <AppContainer>
    <CssBaseline />
    <TopBar>
      <DeveloperBoard />
      <Typography variant="headline" color="inherit">
        Crossroads
      </Typography>
    </TopBar>
    <EndpointDrawer />
    <AppContent>
      <Deployments />
    </AppContent>
  </AppContainer>
)
