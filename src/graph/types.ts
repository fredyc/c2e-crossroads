/* tslint:disable */

/** Date in format YYYY-MM-DD */
export type Date = any

/** Time without date (HH:MM:SS) */
export type Time = any

/** Date in format iso 8601 format */
export type DateTime = any

/** Data in json format */
export type Json = any

/** Input file type for Apollo upload using multipart requests */
export type InputFile = any

export interface InputOrderIndex {
  companyBranches?: string[] | null
  user?: string | null
  orderStatesAND?: OrderStateEnum[] | null
  orderStatesOR?: OrderStateEnum[] | null
  orderStateCategoriesOR?: OrderStateCategoryEnum[] | null
  orderStateCategoryTarget?: OrderStateCategoryTargetEnum | null
  currency?: CurrencyEnum | null
  location?: InputLocationIndex | null
  stateChangeSince?: DateTime | null
  includeOldFinished?: boolean | null
}

export interface InputLocationIndex {
  tableNumber?: number | null
}

export interface InputPaginationParams {
  fromIndex: number
  toIndex: number
}

export interface InputVamStatus {
  email?: string | null
  phone?: string | null
  firstname?: string | null
  lastname?: string | null
  orderId: string
}

export interface InputOrderRecipeCreate {
  order: string
  recipe: string
  orderRecipeState?: OrderRecipeStateEnum | null
  parent?: string | null
  sideDishes?: string[] | null
  associatedRecipes?: string[] | null
  amount?: number | null
}

export interface InputOrderRecipeUpdate {
  order?: string | null
  recipe?: string | null
  orderRecipeState?: OrderRecipeStateEnum | null
  parent?: string | null
  sideDishes?: string[] | null
}

export interface InputOrderDeliveryCreate {
  companyBranch?: string | null
  brand?: string | null
  orderOrigin: OrderOriginEnum
  gps: InputGeoPointCreate
  deliveryType?: DeliveryTypeEnum | null
  address?: string | null
}

export interface InputGeoPointCreate {
  latitude: number
  longitude: number
}

export interface InputExternalOrderCreate {
  companyBranch: string
  orderOrigin: OrderOriginEnum
  deliverAt: DateTime
  gps?: InputGeoPointCreate | null
  address?: string | null
  warePriceType?: WarePriceTypeEnum | null
  deliveryType?: DeliveryTypeEnum | null
  orderRecipes?: InputOrderRecipeCreates[] | null
  customer?: InputCustomerCreate | null
  note?: string | null
  cancelUnpaid?: boolean | null
}

export interface InputOrderRecipeCreates {
  recipe: string
  sideDishes?: string[] | null
  associatedRecipes?: string[] | null
  amount?: number | null
}

export interface InputCustomerCreate {
  firstName?: string | null
  lastName?: string | null
  emails?: InputEmailCreate[] | null
  phones?: InputPhoneCreate[] | null
}

export interface InputEmailCreate {
  email: string
  description?: string | null
  default?: boolean | null
}

export interface InputPhoneCreate {
  phone: string
  description?: string | null
  default?: boolean | null
}

export interface InputOrderCreate {
  companyBranch: string
  orderOrigin: OrderOriginEnum
  warePriceType: WarePriceTypeEnum
  deliveryType?: DeliveryTypeEnum | null
  language?: LanguageEnum | null
  customer: InputCustomerCreate
  gps?: InputGeoPointCreate | null
  address?: string | null
  notification?: InputNotificationCreate | null
  note?: string | null
  deliverAt?: DateTime | null
  location?: InputLocationCreate | null
  callEventId?: string | null
  sector?: string | null
}

export interface InputNotificationCreate {
  sms?: string | null
  chrome?: Json | null
  firebase?: Json | null
}

export interface InputLocationCreate {
  tableNumber?: number | null
}

export interface InputOrderPickupCreate {
  companyBranch: string
  orderOrigin: OrderOriginEnum
}

export interface InputOrderWifiCreate {
  companyBranch: string
  orderOrigin: OrderOriginEnum
  warePriceType: WarePriceTypeEnum
  deliveryType?: DeliveryTypeEnum | null
  sector?: string | null
}

export interface InputOrderPaymentMethodsCreate {
  paymentType: PaymentTypeEnum
  paymentGate: PaymentGateEnum
  price: InputCurrencyCreate
}

export interface InputCurrencyCreate {
  value: number
  currency?: CurrencyEnum | null
}

export interface InputOrderFinish {
  customer?: InputCustomerCreate | null
  paymentMethod?: PaymentTypeEnum | null /** payment method used to finalize order */
  paymentGate?: PaymentGateEnum | null /** payment gate used to finalize order */
  deliverAt?: DateTime | null /** Preferred time of order delivery. In case that argument is null, deliver_at value is computed based on branch opening hours and default delivery times */
  note?: string | null
  confirmedAgreements?: string[] | null
  tips?: number | null /** percentage value of tips */
}

export interface InputOrderStaffImport {
  warePriceType: WarePriceTypeEnum
  companyBranch: string
  acceptedAt?: DateTime | null
  acceptedAtRaw?: string | null
  orderRecipes: InputOrderRecipeCreates[]
  deliveryType?: DeliveryTypeEnum | null
  invoice: string
  invoiceNumber: number
  paymentMethods: InputOrderPaymentMethodsCreate[]
  canceled?: boolean | null
}

export interface InputOrderUpdate {
  deliveryType?: DeliveryTypeEnum | null
  language?: LanguageEnum | null
  customer?: InputCustomerCreate | null
  gps?: InputGeoPointCreate | null
  address?: string | null
  notification?: InputNotificationCreate | null
  note?: string | null
  deliverAt?: DateTime | null
  location?: InputLocationCreate | null
  warePriceType?: WarePriceTypeEnum | null
  tips?: number | null /** percentage value of tips */
}

export interface InputPaymentMethodIndex {
  companyBranch: string
  orderOrigin: OrderOriginEnum
  warePriceType: WarePriceTypeEnum
  paymentType: PaymentTypeEnum
  paymentGate: PaymentGateEnum
  enabled?: boolean | null
}

export interface InputStatisticCreate {
  dateFrom?: Date | null
  dateTo?: Date | null
  dateTimeFrom?: DateTime | null
  dateTimeTo?: DateTime | null
  branches: string[]
  drivers?: string[] | null
}

export interface InputPromoCodeIndex {
  companyBranch?: string | null
  enabled?: boolean | null
}

export interface InputCompanyCreate {
  name: string
  number: string
  vatIdent: string
  account: string
  internalNumber?: string | null
  companyType: CompanyTypeEnum
}

export interface InputCompanyBranchCreate {
  currencies?: CurrencyEnum[] | null
  name: string
  enabled?: boolean | null
  brand: string
  internalNumber: string
}

export interface InputTransportationDeviceCreate {
  companyBranch: string
  deviceType: string
  deviceState: string
  idNumber: string
}

export interface InputBusinessHourCreate {
  weekDay: WeekDayEnum
  openAt: Time
  closeAt: Time
  smsLabel?: InputLabelCreate | null
  webLabel?: InputLabelCreate | null
}

export interface InputLabelCreate {
  translations: InputLabelTranslation[]
  enabled?: boolean | null
}

export interface InputLabelTranslation {
  language: LanguageEnum
  content: string
}

export interface InputBusinessHourSpecificDateCreate {
  companyBranch: string
  openAt?: Time | null
  closeAt?: Time | null
  specificDate: Date
  closed?: boolean | null
  smsLabel?: InputLabelCreate | null
  webLabel?: InputLabelCreate | null
  closedNote?: string | null
  overwritesRegular?: boolean | null
}

export interface InputBusinessHourSpecificCloseCreate {
  companyBranchId: string
  closingInterval: number /** close for defined interval in minutes */
  closedNote?: string | null
}

export interface InputWareCreate {
  companyBranch: string
  nameLabel: InputLabelCreate
  descriptionLabel: InputLabelCreate
  code?: string | null
  internalCode?: string | null
  enabled?: boolean | null
  availabilities?: InputAvailabilityCreate[] | null
}

export interface InputAvailabilityCreate {
  availabilityType?: string | null
  dateFrom?: Date | null
  timeFrom?: Time | null
  dateTo?: Date | null
  timeTo?: Time | null
  available?: boolean | null
  periodic?: boolean | null
  weekDay?: WeekDayEnum | null
  orderOriginCategories?: OrderOriginCategoryEnum[] | null
}

export interface InputWareUpdate {
  code?: string | null
  internalCode?: string | null
  companyBranch?: string | null
  nameLabel?: InputLabelUpdate | null
  descriptionLabel?: InputLabelUpdate | null
  enabled?: boolean | null
}

export interface InputLabelUpdate {
  translations?: InputLabelTranslation[] | null
  enabled?: boolean | null
}

export interface InputWareCategoryCreate {
  companyBranch: string
  wareCategoryType: WareCategoryTypeEnum
  pictogram?: string | null
  nameLabel: string
  internalNoteLabel?: string | null
  enabled?: boolean | null
  public?: boolean | null
  availabilities?: InputAvailabilityCreate[] | null
  maxRecipesCount?: number | null
  minCount?: number | null
  maxCount?: number | null
  recipes?: string[] | null
  orderOriginCategories?: OrderOriginCategoryEnum[] | null
}

export interface InputWareCategoryUpdate {
  companyBranch?: string | null
  pictogram?: string | null
  nameLabel?: string | null
  internalNoteLabel?: string | null
  translations?: InputWareCategoryTranslation[] | null
  wareRequirement?: WareRequirementEnum | null
  wareCategoryType?: WareCategoryTypeEnum | null
  enabled?: boolean | null
  public?: boolean | null
  maxRecipesCount?: number | null
  minCount?: number | null
  maxCount?: number | null
  recipes?: string[] | null
  orderOriginCategories?: OrderOriginCategoryEnum[] | null
}

export interface InputWareCategoryTranslation {
  language: LanguageEnum
  nameLabel?: string | null
  internalNoteLabel?: string | null
}

export interface InputShiftCreate {
  companyBranch: string
  beginAt: DateTime
  endAt: DateTime
  capacity?: number | null
  placeCode: string
}

export interface InputIngredientCreate {
  recipe: string
  stockItem: string
  quantity: number
}

export interface InputRecipeCreate {
  companyBranch: string
  nameLabel: string
  descriptionLabel?: string | null
  internalNote?: string | null
  wareCategoryType: WareCategoryTypeEnum
  enabled?: boolean | null
  ingredients?: InputRecipeIngredient[] | null
  wareCategories?: string[] | null
  sideDishCategories?: string[] | null
  availabilities?: InputAvailabilityCreate[] | null
  video?: string | null
  code?: string | null
  ware?: string | null
  prices: InputRecipePriceCreate[]
  associatedRecipes?: InputRecipeRelationCreate[] | null
  image?: InputFile | null
}

export interface InputRecipeIngredient {
  stockItem: string
  quantity: number
}

export interface InputRecipePriceCreate {
  warePriceType: WarePriceTypeEnum
  price: InputCurrencyCreate
  vat: string
}

export interface InputRecipeRelationCreate {
  associatedRecipe: string
  warePriceType: WarePriceTypeEnum
}

export interface InputRecipeUpdate {
  ware?: string | null
  nameLabel?: string | null
  descriptionLabel?: string | null
  translations?: InputRecipeTranslation[] | null
  enabled?: boolean | null
  internalNote?: string | null
  video?: string | null
  code?: string | null
  ingredients?: InputRecipeIngredient[] | null
  wareCategories?: string[] | null
  sideDishCategories?: string[] | null
  prices?: InputRecipePriceCreate[] | null
  associatedRecipes?: InputRecipeRelationCreate[] | null
  image?: InputRecipeImage | null
}

export interface InputRecipeTranslation {
  language: LanguageEnum
  nameLabel?: string | null
  descriptionLabel?: string | null
}

export interface InputRecipeImage {
  file?: InputFile | null
  delete: boolean
}

export interface InputZoneCreate {
  companyBranch: string
  name?: string | null
  zoneType?: ZoneTypeEnum | null
  zoneColor?: string | null
  enabled?: boolean | null
  geoPolygon?: InputGeoPointCreate[] | null
  availability?: InputAvailabilityCreate | null
  priceRangeRules?: PriceRangeRuleCreate[] | null
  minimalOrderPrice?: InputCurrencyCreate | null
}

export interface PriceRangeRuleCreate {
  price: InputCurrencyCreate
  toPrice: InputCurrencyCreate
}

export interface InputZoneUpdate {
  companyBranch?: string | null
  name?: string | null
  zoneColor?: string | null
  enabled?: boolean | null
  geoPolygon?: InputGeoPointCreate[] | null
  availability?: InputAvailabilityUpdate | null
  priceRangeRules?: PriceRangeRuleCreate[] | null
  minimalOrderPrice?: InputCurrencyCreate | null
}

export interface InputAvailabilityUpdate {
  availabilityType?: string | null
  dateFrom?: Date | null
  timeFrom?: Time | null
  dateTo?: Date | null
  timeTo?: Time | null
  available?: boolean | null
  periodic?: boolean | null
  weekDay?: WeekDayEnum | null
}

export interface InputOrderPaymentMethodCreate {
  order: string
  paymentMethod: string
  price: InputCurrencyCreate
}

export interface InputOrderPaymentMethodUpdate {
  paymentMethodId: string
  price: InputCurrencyCreate
}

export interface InputStockItemCreate {
  companyBranch: string
  wareUnit: WareUnitEnum
  label: string
  code: string
  allowNegative: FieldStateEnum
  plu?: string | null
  quantity?: number | null
  allergens?: AllergenEnum[] | null
  note?: string | null
}

export interface InputStockItemUpdate {
  quantity?: number | null
  label?: string | null
  code?: string | null
  plu?: string | null
  wareUnit?: WareUnitEnum | null
  allergens?: AllergenEnum[] | null
  note?: string | null
  allowNegative?: FieldStateEnum | null
}

export interface InputStockTransferCreate {
  stockItemId: string
  quantity: number
}

export interface InputUserUpdate {
  language?: LanguageEnum | null
}
/** CompanyType enum */
export enum CompanyTypeEnum {
  CLIENT = 'CLIENT',
  AFFILIATE = 'AFFILIATE',
  BRAND = 'BRAND',
}
/** AddressType enum */
export enum AddressTypeEnum {
  BUSINESS = 'BUSINESS',
  PREMISES = 'PREMISES',
  MAIN_ADDRESS = 'MAIN_ADDRESS',
}
/** Currency enum */
export enum CurrencyEnum {
  HOP = 'HOP',
  USD = 'USD',
  PLN = 'PLN',
  EUR = 'EUR',
  CZK = 'CZK',
  ZAR = 'ZAR',
}
/** WeekDay enum */
export enum WeekDayEnum {
  MONDAY = 'MONDAY',
  TUESDAY = 'TUESDAY',
  WEDNESDAY = 'WEDNESDAY',
  THURSDAY = 'THURSDAY',
  FRIDAY = 'FRIDAY',
  SATURDAY = 'SATURDAY',
  SUNDAY = 'SUNDAY',
}
/** OrderStateCategoryTarget enum */
export enum OrderStateCategoryTargetEnum {
  BACKEND = 'BACKEND',
  FE_ADMIN = 'FE_ADMIN',
  FE_KITCHEN = 'FE_KITCHEN',
  DRIVER = 'DRIVER',
  FE_COMPLETION = 'FE_COMPLETION',
}
/** OrderStateCategory enum */
export enum OrderStateCategoryEnum {
  MAIN = 'MAIN',
  NEW_ORDERS = 'NEW_ORDERS',
  IN_CART = 'IN_CART',
  PRODUCTION = 'PRODUCTION',
  PREPARING = 'PREPARING',
  PAYMENT = 'PAYMENT',
  READY_TO_DELIVER = 'READY_TO_DELIVER',
  ON_WAY = 'ON_WAY',
  NOTIFICATION = 'NOTIFICATION',
  DELIVERY = 'DELIVERY',
  FINISHED = 'FINISHED',
  CANCEL = 'CANCEL',
  RECLAMATION = 'RECLAMATION',
  BILLS = 'BILLS',
}
/** Language enum */
export enum LanguageEnum {
  CS = 'CS',
  SK = 'SK',
  PL = 'PL',
  DE = 'DE',
  EN = 'EN',
}
/** Possible mobile plantforms */
export enum MobilePlatformEnum {
  ANDROID = 'ANDROID',
  IOS = 'IOS',
  PWA = 'PWA',
}
/** WareUnit enum */
export enum WareUnitEnum {
  KILOGRAM = 'KILOGRAM',
  GRAM = 'GRAM',
  LITRE = 'LITRE',
  PIECE = 'PIECE',
}
/** PaymentGate enum */
export enum PaymentGateEnum {
  GOPAY = 'GOPAY',
  PAYPAL = 'PAYPAL',
  CREDIT = 'CREDIT',
  CASH = 'CASH',
}
/** PaymentType enum */
export enum PaymentTypeEnum {
  CASH = 'CASH',
  CREDIT = 'CREDIT',
  VOUCHER = 'VOUCHER',
  DISCOUNT = 'DISCOUNT',
}
/** AvailabilityType enum */
export enum AvailabilityTypeEnum {
  MAIN = 'MAIN',
  SPECIFIC = 'SPECIFIC',
}
/** Allergen enum */
export enum AllergenEnum {
  CEREALS_WITH_GLUTTEN = 'CEREALS_WITH_GLUTTEN',
  EGG = 'EGG',
  SHELLFISH = 'SHELLFISH',
  FISH = 'FISH',
  PEANUT = 'PEANUT',
  SOYBEANS = 'SOYBEANS',
  MILK = 'MILK',
  NUT = 'NUT',
  CELERY = 'CELERY',
  MUSTARD = 'MUSTARD',
  SESAME = 'SESAME',
  SULFUROUS = 'SULFUROUS',
  LUPIN = 'LUPIN',
  MOLLUSCS = 'MOLLUSCS',
}
/** WarePriceType enum */
export enum WarePriceTypeEnum {
  DELIVERY = 'DELIVERY',
  INHOUSE = 'INHOUSE',
}
/** FieldState enum */
export enum FieldStateEnum {
  DEFAULT = 'DEFAULT',
  ENABLED = 'ENABLED',
  DISABLED = 'DISABLED',
}
/** ZoneType enum */
export enum ZoneTypeEnum {
  AREA = 'AREA',
  TABLE = 'TABLE',
  SECTOR = 'SECTOR',
  RETURN = 'RETURN',
}
/** DeviceState enum */
export enum DeviceStateEnum {
  WORKING = 'WORKING',
  BROKEN = 'BROKEN',
  FREE = 'FREE',
  BUSY = 'BUSY',
}
/** DeviceType enum */
export enum DeviceTypeEnum {
  CAR = 'CAR',
  PLANE = 'PLANE',
  DRON = 'DRON',
}
/** OrderState enum */
export enum OrderStateEnum {
  IN_CART = 'IN_CART',
  RECEIVED = 'RECEIVED',
  IN_PROGRESS = 'IN_PROGRESS',
  FINISHED = 'FINISHED',
  RECLAMATION = 'RECLAMATION',
  RECLAMATION_HANDLED = 'RECLAMATION_HANDLED',
  UNPAID = 'UNPAID',
  PAID = 'PAID',
  BY_CREDIT_CARD = 'BY_CREDIT_CARD',
  REFUND = 'REFUND',
  SHOULD_BE_PAID = 'SHOULD_BE_PAID',
  CUSTOMER_NOTIFIED = 'CUSTOMER_NOTIFIED',
  NEW = 'NEW',
  ONLINE = 'ONLINE',
  EVALUATED = 'EVALUATED',
  NOT_EVALUATED = 'NOT_EVALUATED',
  SHOWN = 'SHOWN',
  ACCEPTED = 'ACCEPTED',
  READY_TO_PRODUCE = 'READY_TO_PRODUCE',
  IN_PRODUCE = 'IN_PRODUCE',
  MADE = 'MADE',
  ON_WAY = 'ON_WAY',
  STUCK_ON_ROAD = 'STUCK_ON_ROAD',
  LATE = 'LATE',
  DELIVERED = 'DELIVERED',
  CAUSE = 'CAUSE',
  HANDOVER = 'HANDOVER',
  WAITING_FOR_CUSTOMER = 'WAITING_FOR_CUSTOMER',
  CUSTOMER_DID_NOT_COME = 'CUSTOMER_DID_NOT_COME',
  DELIVER_REJECTED = 'DELIVER_REJECTED',
  CANCEL_ON_WAY = 'CANCEL_ON_WAY',
  CANCELED = 'CANCELED',
  BRANCH_CANCEL = 'BRANCH_CANCEL',
  CUSTOMER_CANCEL = 'CUSTOMER_CANCEL',
  READY_TO_DELIVER = 'READY_TO_DELIVER',
  BILL_PASSED = 'BILL_PASSED',
  BILL_PRINTED = 'BILL_PRINTED',
  KITCHEN_TICKET_PRINTED = 'KITCHEN_TICKET_PRINTED',
  HIGH_PRIORITY = 'HIGH_PRIORITY',
  WIFI_DELAYED = 'WIFI_DELAYED',
  ASSIGNED_TO_DELIVER = 'ASSIGNED_TO_DELIVER',
}
/** OrderBillingType enum */
export enum OrderBillingTypeEnum {
  PRICE_SUM = 'PRICE_SUM',
  COMMISSION = 'COMMISSION',
  FEE = 'FEE',
  REWARD_RESULT = 'REWARD_RESULT',
  AFFILIATE = 'AFFILIATE',
}
/** DeliveryType enum */
export enum DeliveryTypeEnum {
  PICKUP = 'PICKUP',
  MESSENGER = 'MESSENGER',
  DRON = 'DRON',
  MAIL = 'MAIL',
}
/** OrderRecipeState enum */
export enum OrderRecipeStateEnum {
  FRESH = 'FRESH',
  PREPARING = 'PREPARING',
  COOKED = 'COOKED',
  CANCELED = 'CANCELED',
}
/** OrderOnlineState enum */
export enum OrderOnlineStateEnum {
  CREATED = 'CREATED',
  PENDING = 'PENDING',
  PAID = 'PAID',
  CANCELED = 'CANCELED',
  COMPLETED = 'COMPLETED',
}
/** OrderOrigin enum */
export enum OrderOriginEnum {
  WEB = 'WEB',
  DAME_JIDLO = 'DAME_JIDLO',
  STAFF = 'STAFF',
  WIFI = 'WIFI',
  BAKER = 'BAKER',
  WEB_PARTNER = 'WEB_PARTNER',
  ENIGOO = 'ENIGOO',
  MOBIL_ANDROID = 'MOBIL_ANDROID',
  MOBIL_IOS = 'MOBIL_IOS',
  MPIZZA = 'MPIZZA',
  MINISITE = 'MINISITE',
  AFFILIATE_PIZZA_ROZVOZ = 'AFFILIATE_PIZZA_ROZVOZ',
  AFFILIATE_ROZVOZ_PIZZY_ZDARMA = 'AFFILIATE_ROZVOZ_PIZZY_ZDARMA',
  AFFILIATE_ROZVOZ_JIDLA = 'AFFILIATE_ROZVOZ_JIDLA',
  CALL_CENTER = 'CALL_CENTER',
  PWA = 'PWA',
  WEB_MENU = 'WEB_MENU',
}
/** SectorType enum */
export enum SectorTypeEnum {
  AREA = 'AREA',
  TABLE = 'TABLE',
}
/** ShiftUserState enum */
export enum ShiftUserStateEnum {
  ACCEPTED = 'ACCEPTED',
  REJECTED = 'REJECTED',
  WAITING_FOR_RESPONSE = 'WAITING_FOR_RESPONSE',
}
/** OrderOriginCategory enum */
export enum OrderOriginCategoryEnum {
  DOVEZE = 'DOVEZE',
  WIFI = 'WIFI',
  STAFF = 'STAFF',
  WEBSITE = 'WEBSITE',
}
/** WareCategoryType enum */
export enum WareCategoryTypeEnum {
  MAIN = 'MAIN',
  SIDE_DISH = 'SIDE_DISH',
  COVER = 'COVER',
  TIP = 'TIP',
  MIXIN = 'MIXIN',
}
/** Possible action types of subscription */
export enum SubscriptionActionType {
  CREATE = 'CREATE',
  UPDATE = 'UPDATE',
  DESTROY = 'DESTROY',
  REMOVE = 'REMOVE',
}
/** SettingType enum */
export enum SettingTypeEnum {
  CONFIRMATION = 'CONFIRMATION',
  ADMIN_ORDER_LISTING = 'ADMIN_ORDER_LISTING',
  RULES = 'RULES',
  URL = 'URL',
  GRAPHIC = 'GRAPHIC',
  OPTIONS = 'OPTIONS',
}
/** WareRequirement enum */
export enum WareRequirementEnum {
  NO_LIMIT = 'NO_LIMIT',
  OPTIONAL = 'OPTIONAL',
  MAX_ONE = 'MAX_ONE',
  MANDATORY = 'MANDATORY',
}
/** DeliveryState enum */
export enum DeliveryStateEnum {
  FRESH = 'FRESH',
  ON_WAY = 'ON_WAY',
  DELIVERED = 'DELIVERED',
  CANCELED = 'CANCELED',
  DRIVER_CHANGED = 'DRIVER_CHANGED',
}
export namespace GDeployment {
  export type Variables = {}

  export type Query = {
    __typename?: 'Query'
    brands: Brands[]
  }

  export type Brands = {
    __typename?: 'Company'
    id: string
    name: string
    wac: string
    subdomain: string
    branches: Branches[]
  }

  export type Branches = {
    __typename?: 'CompanyBranch'
    id: string
    name: string
    enabled: boolean
    wac: string
    urlPath: string
    address: Address
  }

  export type Address = {
    __typename?: 'Address'
    id: string
    location?: Location | null
  }

  export type Location = {
    __typename?: 'JsonAddress'
    street?: string | null
    houseNumber?: string | null
    city?: string | null
  }
}
