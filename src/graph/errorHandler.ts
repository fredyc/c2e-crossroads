import { ErrorResponse, onError } from 'apollo-link-error'

export const errorHandler = ({
  graphQLErrors,
  networkError,
  operation,
}: ErrorResponse) => {
  if (graphQLErrors) {
    graphQLErrors.map(({ message, locations, path }) =>
      // tslint:disable-next-line:no-console
      console.log(
        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
      ),
    )
  }
  if (networkError) {
    // tslint:disable-next-line:no-console
    console.log(`[Network error]: ${networkError}`)
  }
}

export const createErrorLink = () => onError(errorHandler)
