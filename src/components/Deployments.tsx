import { config } from 'config'
import React from 'react'

import { AppState } from './AppState'
import { Client } from './Client'
import { Deployment } from './Deployment'

interface IProps {}

export const Deployments: React.SFC<IProps> = props => (
  <AppState>
    {state => (
      <>
        {config.map(dep => (
          <Client
            key={dep.name}
            name={dep.name}
            endpoint={dep.endpoint.url}
            headers={dep.endpoint.headers}
          >
            {state.endpoints.includes(dep.name) && (
              <Deployment targets={dep.targets} />
            )}
          </Client>
        ))}
      </>
    )}
  </AppState>
)
