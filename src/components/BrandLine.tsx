import { Divider, ExpansionPanel, ExpansionPanelSummary, Typography } from '@material-ui/core'
import { ExpandMore } from '@material-ui/icons'
import { ITarget } from 'config'
import React from 'react'
import styled from 'styled-components'

import { GDeployment } from 'graph/types'

import { TargetButton } from './TargetButton'

interface IProps {
  brand: GDeployment.Brands
  targets: ITarget[]
}

const Wrap = styled.div`
  margin: 1rem;
`

const Panel = styled(ExpansionPanel)`
  width: 100%;
`

const Column = styled.div`
  flex-basis: 33.33%;
`

const TargetButtonSpaced = styled(TargetButton)`
  margin-right: 1rem !important;
`

export const BrandLine: React.SFC<IProps> = ({ brand, targets, children }) => (
  <Wrap>
    <Panel>
      <ExpansionPanelSummary expandIcon={<ExpandMore />}>
        <Column>
          <Typography variant="headline">{brand.name}</Typography>
        </Column>
        <Column>
          {targets.map(target => (
            <TargetButtonSpaced
              color="primary"
              url={makeBrandUrl(brand, target)}
              key={target.label}
            >
              {target.label}
            </TargetButtonSpaced>
          ))}
        </Column>
        <Column>
          <Typography variant="display1">{`#${brand.id}`}</Typography>
        </Column>
      </ExpansionPanelSummary>
      {children}
      <Divider />
    </Panel>
  </Wrap>
)

export function makeBrandUrl(brand: GDeployment.Brands, target: ITarget) {
  const url = new URL(target.url)
  if (target.subdomains && brand.subdomain) {
    url.host = `${brand.subdomain}.${url.host}`
  } else if (brand.wac) {
    url.searchParams.append('wac', brand.wac)
  }
  return String(url)
}
