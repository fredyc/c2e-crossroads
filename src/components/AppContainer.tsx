import styled from 'styled-components'

export const AppContainer = styled.div`
  flex-grow: 1;
  z-index: 1;
  overflow: hidden;
  position: relative;
  display: flex;
`
