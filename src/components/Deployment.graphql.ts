import gql from 'graphql-tag'

export const DeploymentQuery = gql`
  query GDeployment {
    brands: companies(companyType: BRAND) {
      id
      name
      wac
      subdomain
      branches: companyBranches {
        id
        name
        enabled
        wac
        urlPath
        address {
          id
          location {
            street
            houseNumber
            city
          }
        }
      }
    }
  }
`
