import styled from 'styled-components'

export const AppContent = styled.main`
  flex-grow: 1;
  min-width: 0;
  padding: 5rem 1rem;
`
