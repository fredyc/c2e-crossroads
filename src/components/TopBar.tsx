import { AppBar, Toolbar } from '@material-ui/core'
import React from 'react'
import styled from 'styled-components'

interface IProps {}

const AppBarStyled = styled(AppBar)`
  z-index: 10000;
`

export const TopBar: React.SFC<IProps> = ({ children }) => (
  <AppBarStyled position="absolute">
    <Toolbar>{children}</Toolbar>
  </AppBarStyled>
)
