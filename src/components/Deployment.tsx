import { ITarget } from 'config'
import React from 'react'
import { Query } from 'react-apollo'

import { BrandLine } from 'components/BrandLine'
import { GDeployment } from 'graph/types'

import { BranchGrid } from './BranchGrid'
import { DeploymentQuery } from './Deployment.graphql'

interface IProps {
  targets: ITarget[]
}

export const Deployment: React.SFC<IProps> = ({ targets }) => (
  <>
    <Query query={DeploymentQuery} fetchPolicy="cache-and-network">
      {({ data, loading, error }) => {
        if (loading) {
          return <h3>Fetching...</h3>
        }
        if (error) {
          return <pre>{error}</pre>
        }
        const { brands } = data as GDeployment.Query
        return brands.map(brand => (
          <BrandLine brand={brand} key={brand.id} targets={targets}>
            <BranchGrid brand={brand} targets={targets} />
          </BrandLine>
        ))
      }}
    </Query>
  </>
)
