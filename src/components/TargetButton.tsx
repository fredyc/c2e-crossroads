import { Button } from '@material-ui/core'
import { ButtonProps } from '@material-ui/core/Button'
import React from 'react'

interface IProps extends Omit<ButtonProps, 'target'> {
  url: string
  children: string
}

export const TargetButton: React.SFC<IProps> = ({
  url,
  children,
  ...props
}) => (
  <Button href={url} variant="contained" target="_blank" {...props}>
    {children}
  </Button>
)
