import { InMemoryCache } from 'apollo-cache-inmemory'
import { persistCache } from 'apollo-cache-persist'
import { ApolloClient } from 'apollo-client'
import { ApolloLink } from 'apollo-link'
import { setContext } from 'apollo-link-context'
import { onError } from 'apollo-link-error'
import { createHttpLink } from 'apollo-link-http'
import localforage from 'localforage'
import React from 'react'
import { ApolloProvider } from 'react-apollo'

import { errorHandler } from 'graph/errorHandler'

interface IProps {
  name: string
  endpoint: string
  headers: object
}

export class Client extends React.Component<IProps> {
  client = createApolloClient(this.props)
  render() {
    return (
      <ApolloProvider client={this.client}>
        {this.props.children}
      </ApolloProvider>
    )
  }
}

export function createApolloClient({ name, endpoint, headers }: IProps) {
  const cache = new InMemoryCache({
    dataIdFromObject: (o: { __typename?: string; id?: string }) => {
      if (o.__typename != null && o.id != null) {
        return `${o.__typename}-${o.id}`
      }
      return null
    },
  })

  // @ts-ignore
  persistCache({ cache, storage: localforage, key: `crossroads-${name}` })

  const httpLink = createHttpLink({
    uri: endpoint,
    credentials: 'same-origin',
  })

  const authLink = setContext(() => ({
    headers,
  }))

  const errorLink = onError(errorHandler)

  const link = ApolloLink.from([errorLink, authLink, httpLink])

  return new ApolloClient({ link, cache })
}
