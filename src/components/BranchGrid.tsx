import { Card, CardContent, CardHeader } from '@material-ui/core'
import { ITarget } from 'config'
import React from 'react'
import Masonry from 'react-masonry-component'
import styled from 'styled-components'

import { GDeployment } from 'graph/types'

import { makeBrandUrl } from './BrandLine'
import { TargetButton } from './TargetButton'

interface IProps {
  brand: GDeployment.Brands
  targets: ITarget[]
}

const SpacedCard = styled(Card)`
  margin: 0.4rem;
  width: 15rem;
`

export const BranchGrid: React.SFC<IProps> = ({ brand, targets }) => (
  <Masonry>
    {brand.branches.map(branch => (
      <SpacedCard>
        <CardHeader title={branch.name} subheader={makeAddress(branch)} />
        <CardContent>
          {targets.map(target => (
            <TargetButton
              color="default"
              url={makeBranchUrl(brand, branch, target)}
              key={target.label}
            >
              {target.label}
            </TargetButton>
          ))}
        </CardContent>
      </SpacedCard>
    ))}
  </Masonry>
)

function makeAddress(branch: GDeployment.Branches) {
  const { location } = branch.address
  if (location) {
    return `${location.street} ${location.houseNumber}, ${location.city}`
  }
  return 'Unknown address'
}

export function makeBranchUrl(
  brand: GDeployment.Brands,
  branch: GDeployment.Branches,
  target: ITarget,
) {
  const url = new URL(makeBrandUrl(brand, target))
  if (target.subdomains && branch.urlPath) {
    url.pathname = branch.urlPath
  } else if (branch.wac) {
    url.searchParams.append('wac', branch.wac)
  }
  return String(url)
}
