import localforage from 'localforage'
import { autorun, IObservableObject, observable, set, toJS, when } from 'mobx'
import { Observer } from 'mobx-react'
import React from 'react'

const defaultState = {
  endpoints: ['prod'],
}

interface IProps {
  children(state: TAppState): ReactNode
  observe?: boolean
}

const currentState = observable(defaultState)
const stateIsReady = observable.box(false)

const persistKey = 'crossroads'
autorun(() => {
  if (stateIsReady.get()) {
    localforage.setItem(persistKey, toJS(currentState))
  }
})
localforage.getItem(persistKey).then(item => {
  if (item) {
    // @ts-ignore
    set(currentState, item)
  }
  stateIsReady.set(true)
})

export type TAppState = typeof defaultState & IObservableObject

const { Consumer } = React.createContext<TAppState>(currentState)

export class AppState extends React.Component<IProps> {
  componentDidMount() {
    when(() => stateIsReady.get(), () => this.forceUpdate())
  }
  render() {
    if (!stateIsReady.get()) {
      return null
    }
    const { children, observe = true } = this.props
    if (observe) {
      return (
        <Consumer>
          {state => <Observer>{() => children(state)}</Observer>}
        </Consumer>
      )
    }
    return <Consumer>{children}</Consumer>
  }
}
