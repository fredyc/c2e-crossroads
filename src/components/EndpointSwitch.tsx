import { ListItem, ListItemSecondaryAction, ListItemText, Switch } from '@material-ui/core'
import React from 'react'

import { AppState } from './AppState'

interface IProps {
  endpoint: string
  label: string
}

export const EndpointSwitch: React.SFC<IProps> = ({ endpoint, label }) => (
  <AppState>
    {state => (
      <ListItem>
        <ListItemText primary={label} />
        <ListItemSecondaryAction>
          <Switch
            checked={state.endpoints.includes(endpoint)}
            onChange={() => {
              const idx = state.endpoints.indexOf(endpoint)
              if (idx >= 0) {
                state.endpoints.splice(idx, 1)
              } else {
                state.endpoints.push(endpoint)
              }
            }}
          />
        </ListItemSecondaryAction>
      </ListItem>
    )}
  </AppState>
)
