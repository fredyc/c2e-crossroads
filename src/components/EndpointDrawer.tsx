import { Drawer, List, ListSubheader } from '@material-ui/core'
import { DrawerProps } from '@material-ui/core/Drawer'
import { config } from 'config'
import React from 'react'
import styled from 'styled-components'

import { EndpointSwitch } from './EndpointSwitch'

interface IProps {}

const ListStyled = styled(List)`
  width: 100%;
  max-width: 360px;
`

const DrawerStyled = styled((props: DrawerProps) => (
  <Drawer {...props} classes={{ paper: 'paper' }} />
))`
  position: relative;
  top: 4rem;
  & .paper {
    position: relative;
    width: 200px;
  }
`

export const EndpointDrawer: React.SFC<IProps> = props => (
  <DrawerStyled anchor="left" variant="permanent">
    <ListStyled>
      <List
        component="nav"
        subheader={<ListSubheader component="div">Deployments</ListSubheader>}
      >
        {config.map(dep => (
          <EndpointSwitch
            key={dep.name}
            endpoint={dep.name}
            label={dep.label || dep.name}
          />
        ))}
      </List>
    </ListStyled>
  </DrawerStyled>
)
